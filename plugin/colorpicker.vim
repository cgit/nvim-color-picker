let s:script_dir = expand("<sfile>:p:h")

if !exists('g:colorpicker_cmd')
  let g:colorpicker_cmd = printf("%s/%s", s:script_dir, "pickcolors.py")
endif

command! -nargs=* PickColor call PickColorFor(<f-args>)

function! PickColorFor(hlgroup, fgbg)
  let opts = {}

  let opts.stdout_buffered = v:false
  let opts.hlgroup = a:hlgroup
  let opts.fgbg = a:fgbg

  function opts.on_stdout(id, data, ev) dict
    if len(a:data[0]) > 0
      if a:data[0] == "END"
        call jobstop(a:id)
      else
        exec printf("hi %s %s=%s", self.hlgroup, self.fgbg, a:data[0])
      endif
    endif
  endfunction

  let job = jobstart(g:colorpicker_cmd, opts)
endfunction
