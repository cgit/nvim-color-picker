#!/usr/bin/python3

import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor

from PyQt5.QtWidgets import QApplication, QColorDialog


class ColorPicker(object):

    def __init__(self):
        self.app = QApplication(sys.argv)

    def get_color(self):
        color_dialog = QColorDialog(parent=None)
        color_dialog.currentColorChanged.connect(self.on_color_changed)
        color_dialog.finished.connect(self.close_on_finish)
        color_dialog.exec_()

    def on_color_changed(self, color):
        # Convert color to RGB format
        # red, green, blue, _ = color.getRgb()
        hex_color = color.name(QColor.HexRgb)
        print(hex_color, flush=True)
        # print(f"Selected Color: RGB({red}, {green}, {blue})")

    def run(self):
        self.get_color()
        sys.exit(self.app.exec_())

    def close_on_finish(self, event):
        print("END")
        sys.exit(0)

if __name__ == "__main__":
    picker = ColorPicker()
    picker.run()
